/**
 * Created by minus on 12/20/14.
 */

function geoMetric() {

    this.ctx = document.getElementById("myCanvas1").getContext("2d");

    this.getValues = function (){
        var output = {};
        $.each($("input[type='range']"), function(x,y) {output[$(y).attr("id")] = parseInt($(y).val()); });
        return output;
    };

    this.saveValues = function(){
        var CurrentValues = this.getValues();

        this.yMin = CurrentValues.yMin;
        this.yMax = CurrentValues.yMax;
        this.yMiddle = CurrentValues.yMiddle;

        this.xMin = CurrentValues.xMin;
        this.xMax = CurrentValues.xMax;
        this.xMiddle = CurrentValues.xMiddle;

        this.stepX = CurrentValues.stepX;
        this.stepY = CurrentValues.stepY;
    };

    this.draw = function (){
        this.clearDrawing();
        this.saveValues();

        for (var x = this.xMin, y = this.yMiddle; x <= this.xMiddle, y >= this.yMin; x+=this.stepX, y-=this.stepY){
            var a = {x: x, y: this.yMiddle};
            var b = {x: this.xMiddle, y: y};
            this.lineBetween(a, b);
        }

        for (var x = this.xMiddle, y = this.yMax; x <= this.xMax, y >= this.yMiddle; x+=this.stepX, y-=this.stepY){
            var a = {x: x, y: this.yMiddle};
            var b = {x: this.xMiddle, y: y};
            this.lineBetween(a, b);
        }

        for (var x = this.xMin, y = this.yMiddle; x <= this.xMiddle, y <= this.yMax; x+=this.stepX, y+=this.stepY){
            var a = {x: x, y: this.yMiddle};
            var b = {x: this.xMiddle, y: y};
            this.lineBetween(a, b);
        }

        for (var x = this.xMiddle, y = this.yMin; x <= this.xMax, y <= this.yMiddle; x+=this.stepX, y+=this.stepY){
            var a = {x: x, y: this.yMiddle};
            var b = {x: this.xMiddle, y: y};
            this.lineBetween(a, b);
        }

        /*for (var x = 0, y = 2000; x <= 1000, y >= 1000; x++, y-=10){
         var a = {x: x, y: 1000};
         var b = {x: 1000, y: y};
         lineBetween(a, b);
         }*/
    };

    this.lineBetween = function (a, b){
        /*var grd = this.ctx.createRadialGradient(a.x, a.y, 0, b.x, b.y, 50);
        grd.addColorStop(0, "rgba(32,120,13,.6)");
        grd.addColorStop(1, "transparent");
        this.ctx.fillStyle = grd;*/
        this.ctx.beginPath();
        this.ctx.moveTo(a.x, a.y);
        this.ctx.lineTo(b.x, b.y);
        /*this.ctx.moveTo(a.x, a.y);
        this.ctx.lineTo(b.x-100, b.y-100);
        this.ctx.arcTo(a.x, a.y, b.x, b.y,500);
        this.ctx.lineTo(b.x, b.y);*/
        /*this.ctx.lineTo(150,120);
        this.ctx.arc(a.x, a.y,200,200,200, false);
        this.ctx.arc(b.x, b.y,200,100,200, false);*/
        //this.ctx.fill();
        this.ctx.closePath();

        var lingrad = this.ctx.createLinearGradient(0,0,0,2000);
        lingrad.addColorStop(0, '#00ABEB');
        lingrad.addColorStop(0.5, '#89f18e');
        /*lingrad.addColorStop(0.5, '#66CC00');*/
        lingrad.addColorStop(1, '#66CC00');

        //this.ctx.fillStyle = lingrad;
        this.ctx.strokeStyle = lingrad;

        /*this.ctx.moveTo(a.x, a.y);
        this.ctx.lineTo(b.x, b.y);*/
        /*this.ctx.fill();*/
        //this.ctx.strokeStyle = $("#lineColor").val();
        this.ctx.stroke();
    };

    this.clearDrawing = function (){
        this.ctx.clearRect(0, 0, window.geoMetric.ctx.canvas.width, window.geoMetric.ctx.canvas.height);

    };

    this.resetValues = function(){
        var defaultValues = JSON.parse('"{"xMax":2000,"xMin":0,"xMiddle":1000,"yMax":2000,"yMin":0,"yMiddle":1000,"stepX":10,"stepY":10}"');
        $.each(defaultValues, function(k,v){$("#" + k).val(v);});
    }

    this.exportValues = function () {
        /*var data = JSON.stringify(window.geoMetric.getValues());
        window.open('data:text/csv;charset=utf-8,' + data);*/
        downloadFile();
    };

    this.downloadImage = function(){
        var href = this.ctx.canvas.toDataURL('image/png');
        console.log(href);
        $("#downloadImage").text("Download ready").attr("href", href);
    };
}