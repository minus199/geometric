/**
 * Created by minus on 12/21/14.
 */

const MIME_TYPE = 'text/plain';

var cleanUp = function(a) {
    a.textContent = 'Downloaded';
    a.dataset.disabled = true;

    // Need a small delay for the revokeObjectURL to work properly.
    setTimeout(function() {
        window.URL.revokeObjectURL(a.href);
    }, 1500);
};

var downloadFile = function() {
    window.URL = window.webkitURL || window.URL;

   /* var prevLink = output.querySelector('a');
    if (prevLink) {
        window.URL.revokeObjectURL(prevLink.href);
        ;
    }*/

    //var bb = new Blob([typer.textContent], {type: MIME_TYPE});

    var content = JSON.stringify(window.geoMetric.getValues());
    var bb = new Blob([content], {type: MIME_TYPE});

    var downloadLink = $("#downloadLink");
    downloadLink.text("Download ready").attr("download", "geoMetric_values.txt").attr("href", window.URL.createObjectURL(bb)).attr('draggable', true);
    //downloadLink.dataset.downloadurl = [MIME_TYPE, downloadLink.attr("download"), downloadLink.attr("href")].join(':');

    downloadLink.click = function(e) {
        if ('disabled' in this.dataset) {
            return false;
        }

        cleanUp(this);
    };
};
